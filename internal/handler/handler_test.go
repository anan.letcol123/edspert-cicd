package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestSumHandler(t *testing.T) {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	body := sumBody{
		X: 9,
		Y: 3,
	}

	jsonBody, _ := json.Marshal(body)

	fmt.Println(jsonBody)

	ctx.Request = httptest.NewRequest("POST", "/", bytes.NewBuffer(jsonBody))

	SumHandler(ctx)

	if w.Code != http.StatusOK {
		t.Fatalf("Status code is not 200 but got %d", w.Code)
	}

	bodyResponse := make(map[string]interface{})

	err := json.Unmarshal(w.Body.Bytes(), &bodyResponse)
	if err != nil {
		t.Fatalf("Status code is not 200 but got %d", w.Code)
	}

}
