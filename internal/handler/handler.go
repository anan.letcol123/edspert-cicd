package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type sumBody struct {
	X float64 `json	:"x"`
	Y float64 `json	:"y"`
}

func SumHandler(c *gin.Context) {
	var body sumBody
	if err := c.ShouldBindJSON(&body); err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"Result": body.X + body.Y,
	})
}
